==========================
Python Package sbrfeatures
==========================

This package was developed within a research project aiming at monitoring small,
unstaffed wastewater treatment plants of the type sequencing batch reactor (SBR)
with unmaintained sensors (see [Schneider2019]_).

The sbrfeatures package is designed to automatically analyse an online
measurement signal by first smoothing it, creating a spline representation and
than search for specific features such as a minimum, maximum or a ramp.

.. [Schneider2019] Schneider, M. Y., Carbajal, J. P., Furrer, V., Sterkele, B., Maurer, M., Villez, K. Beyond signal quality: The value of unmaintained pH, dissolved oxygen, and oxidation-reduction potential sensors for remote performance monitoring of on-site sequencing batch reactors, Water Research, Volume 161, 2019, Pages 639-651, DOI . doi.org/10.1016/j.watres.2019.06.007

Preprint: https://doi.org/10.31224/osf.io/ndm7f

Quickstart
==========

Install the latest version of sbrfeatures::

    pip install "git+https://gitlab.com/sbrml/sbrfeatures.git"

Further Information
===================

Check beyondsignalquality_ subproject for examples with the data_ from a two
population equivalent sequencing batch reactor.

.. _beyondsignalquality: https://gitlab.com/sbrml/beyondsignalquality

.. _data: https://doi.org/10.25678/0000dd

For more information check the documentation_ .

.. _documentation: https://sbrml.gitlab.io/sbrfeatures
