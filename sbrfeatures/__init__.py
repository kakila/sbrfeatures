# vim: set tabstop=4
# __init__.py
#!/usr/bin/env python3
""" Index file for module SBRfetures"""

__copyright__ = ["Copyright (C) 2018 Juan Pablo Carbajal",
    "Copyright (C) 2018 Maraine Yvonne Schneider"]
__credits__ = ["Juan Pablo Carbajal",
    "Mariane Yvonne Schneider"]
__license__ = """This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""
__version__ = "0.1.0+dev"
__author__ = ["Juan Pablo Carbajal",
    "Mariane Yvonne Schneider"]
__maintainer__ = ["Juan Pablo Carbajal",
    "Mariane Yvonne Schneider"]
__email__ = ["ajuanpi+dev@gmailcom",
    "myschneider@meiru.ch"]
__status__ = "Development"
