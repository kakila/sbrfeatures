# vim: set tabstop=4
# features_O2.py
#!/usr/bin/env python3

""" Implementation of features for the Oxymeter signal."""

# Copyright (C) 2018 Mariane Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>

############
## Imports
# built-ins

# 3rd party
import numpy as np

# user
from .basicfunctions import (
    spline,
    splev,
    inflection_points,
    smooth,
    signal_extrema
)
############

## Features
def aeration_ramp(time, signal, *, minslope=None, t_interval=None, smoother=None):
    """ Point during the aeration phase at which the low-passed Oxymeter signal
        derivative reaches a maximum.

        The aeration ramp is a maximum in the derivative of the Oxymeter signal
        during the aeration phase. The signal can be noisy and present
        oscillations due to the aeration method. Hence the maximum is searched
        in a smoothed version of the signal.

        The signal is assumed to be sampled with constant sampling rate.

        This feature is meant to be extracted from Oxymeter signals during the
        aeration phase.

        Arguments
        ---------
        time : array_like
            Time values.
        signal : array_like
            Oxymeter signal during aeration phase.

        Keyword arguments
        -----------------
        minslope : float, optional
            Minimum ramp slope to consider the ramp valid. If none is given
            use -Inf.
        t_interval : array_like, optional
            Initial and final time indicating the valid time interval. Ramps
            are not searched outside of this interval. Default is the whole
            input time
        smoother : function, optional
            A function used to smooth the signal with the signature
            `smooth_signal = smoother(time, signal)`
            The default smoother is the function :func:`basicfunctions.smooth`

        Returns
        -------
        float (same dtype as input time)
            Timestamp of th ramp, i.e. maximum of the derivative
        tuple
            Features of the signal at ramp (signal value, maximum of derivative)
        array_like
            The smoothed signal used for the search
    """
    if smoother is None:
        smoother = smooth
    signal_ = smoother(time, signal)

    # Build spline on smooth signal
    spl = spline(time, signal_)

    # Use only a subset of the time vector
    if t_interval is not None:
        ininterval = (time >= t_interval[0]) & (time <= t_interval[1])
        t_short = time[ininterval]
    else:
        t_short = time

    tm = np.array([])
    ym = np.array([])
    dm = np.array([])
    # Find inflection points of spline in t_short
    tm_, ym_, is_saddle = inflection_points(t_short, spl)
    if tm_ is not None:
        # remove saddle points
        no_saddle = ~is_saddle
        if np.any(no_saddle):
            tm = tm_[no_saddle]
            ym = ym_[no_saddle]
            dm = splev(tm, spl, der=1)

    if tm.size == 0:
        # append maximum of the derivative
        _, tm_, dm_, _ = signal_extrema(t_short, spl, der=1)
        if tm_ is not None:
            ym = np.append(ym, splev(tm_, spl))
            dm = np.append(dm, dm_)
            tm = np.append(tm, tm_)

    if minslope is None:
        minslope = -np.inf

    if tm.size > 0:
        # consider only those with slope higher than given minimum
        ishigher = (dm >= minslope)
        dm = dm[ishigher]
        tm = tm[ishigher]
        ym = ym[ishigher]

    # find maximum derivative among candidates
    if tm.size > 0:
        idx = np.argmax(dm)
        tm = tm[idx]
        ym = ym[idx]
        dm = dm[idx]

    # If nothing is left return None
    if tm.size == 0:
        tm = None
        ym = None
        dm = None

    return tm, (ym, dm), signal_

if __name__ == '__main__':
    # This main is exceuted using 'python -m sbrfetures.features_O2'
    import matplotlib.pyplot as plt

    time = np.linspace(-1, 1, 200)
    print("Running demo")
    plt.figure()
    x = np.tanh(5*time)
    xn = x + 0.1 * np.random.randn(*time.shape)
    tmax, fea_ramp, xs = aeration_ramp(time, x, minslope=0.5)
    xramp, _ = fea_ramp
    plt.plot(time, x, '-', label='noiseless')
    plt.plot(time, xn, 'o', markerfacecolor='none', label='noisy')
    plt.plot(time, xs, '--', label='smoothed')
    plt.plot(tmax, xramp, 'o', label='max der')
    plt.xlabel('time')
    plt.ylabel('signal')
    plt.legend()
    plt.show()
